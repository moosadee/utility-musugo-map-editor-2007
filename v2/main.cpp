#include <iostream>
#include <fstream>
#include <string>
#include <allegro.h>
#include "rjm_buttonClass.h"
#include "rjm_gridClass.h"
using namespace std;

const int SCR_W = 1024;
const int SCR_H = 768;
const int TILESET_W_H = 32;
const int TOOLBAR_H = 150;
const int BUTTON_AMT = 10;
//const int max_x = (1024 / 32)*2;    //64
//const int max_y = (768 / 32)*2;     //48

/*
To do:
= Add layer support
= Add animation support
= Add saving
= Add loading
*/

volatile long speed_counter = 0;
void increment_speed_counter();
void init_allegro();
void init_crap(bool);

int main(int argc, char *argv[])
{
    int max_x, max_y;
    bool fullscreen = false;
    string tileset_filename;
    bool done = false;
    bool saving = false;
    bool loading = false;
    string filename;
    cout<<"\n MUSUGO MAP EDITOR - Rachel J. Morris "<<endl;
    cout<<"Please enter the following!"<<endl;
    cout<<"----------------------------------------"<<endl;
    cout<<"enter max X, or -1 for help."<<endl;
    cout<<"----------------------------------------"<<endl;
    cin>>max_x;
    while ( max_x <= 0 )
    {
        system("cls");
        cout<<"--------------------------------------------------------"<<endl;
        cout<<"Maximum X and Y will be the number of tiles (not pixels)"<<endl;
        cout<<"in that direction.  For example, if your resolution is"<<endl;
        cout<<"640x480, then you'll have 20x15 tiles on one screen."<<endl;
        cout<<"You might multiply that by a number to have a bigger"<<endl;
        cout<<"map that will scroll."<<endl<<endl;
        cout<<"Resolution\tTiles on screen"<<endl;
        cout<<"640x480\t\t20x15"<<endl;
        cout<<"800x600\t\t25x18.75"<<endl;
        cout<<"1024x768\t32x24"<<endl;
        cout<<"--------------------------------------------------------"<<endl;
        cout<<"enter max X: ";
        cin>>max_x;
    }
    cout<<"----------------------------------------"<<endl;
    cout<<"enter max Y or -1 for help."<<endl;
    cout<<"----------------------------------------"<<endl;
    cin>>max_y;
    while ( max_y <= 0 )
    {
        system("cls");
        cout<<"--------------------------------------------------------"<<endl;
        cout<<"Maximum X and Y will be the number of tiles (not pixels)"<<endl;
        cout<<"in that direction.  For example, if your resolution is"<<endl;
        cout<<"640x480, then you'll have 20x15 tiles on one screen."<<endl;
        cout<<"You might multiply that by a number to have a bigger"<<endl;
        cout<<"map that will scroll."<<endl<<endl;
        cout<<"Resolution\tTiles on screen"<<endl;
        cout<<"640x480\t\t20x15"<<endl;
        cout<<"800x600\t\t25x18.75"<<endl;
        cout<<"1024x768\t32x24"<<endl;
        cout<<"--------------------------------------------------------"<<endl;
        cout<<"max X = "<<max_x<<endl;
        cout<<"enter max Y: ";
        cin>>max_y;
    }
    cout<<"\nenter the filename of the tileset (include .bmp at the end): ";
    cin>>tileset_filename;
    init_allegro();
    BITMAP *buffer = create_bitmap(SCR_W, SCR_H);
    BITMAP *graphics = load_bitmap(tileset_filename.c_str(), NULL);
    bool show_about = false;
    bool click = false;
    int brushcode = 0;
    int offset_y = 150;
    int offset_x = 0;
    int tile_offset = 0;
    int brush_size = 1;
    int scroll_speed = 4;
    int tileset_amt = (graphics->w) / 32;
    rjm_gridClass grid[3][max_x][max_y];
//    rjm_gridClass top[max_x][max_y];
//    rjm_gridClass anim[max_x][max_y];
    rjm_gridClass tileset[tileset_amt];
    rjm_buttonClass button[BUTTON_AMT];
    bool show_grid = true;
    int layer = 0;
    bool key_down = false;

    for (int i=0; i<BUTTON_AMT; i++)
    {
        button[i].init(i);
    }

    for (int j=0; j<max_y; j++)
    {
        for (int i=0; i<max_x; i++)
        {
            for (int k=0; k<3; k++)
            {
            grid[k][i][j].x = i*32+32;
            grid[k][i][j].y = j*32+32;
            grid[k][i][j].w = 32;
            grid[k][i][j].h = 32;
            grid[k][i][j].codex = 0;
            grid[k][i][j].codey = 0;
            }
        }
    }
    for (int i=0; i<tileset_amt; i++)
    {
        tileset[i].x = i*32;
        tileset[i].y = TOOLBAR_H - 32;
        tileset[i].w = 32;
        tileset[i].h = 32;
        tileset[i].codex = i*32;
        tileset[i].codey = 0;
    }
    while (!done)
    {
        while (speed_counter > 0)
        {
            if (key[KEY_F4]) { done = true; }                   //Quit
            if (key[KEY_F5])
            {
                if ( fullscreen ) { set_gfx_mode(GFX_AUTODETECT_WINDOWED, SCR_W, SCR_H, 0, 0); fullscreen = false; }
                else { set_gfx_mode(GFX_AUTODETECT, SCR_W, SCR_H, 0, 0); fullscreen = true; }
            }
            //Adjusts offset
            if (key[KEY_UP]) { offset_y += scroll_speed; }
            else if (key[KEY_DOWN]) { offset_y -= scroll_speed; }
            if (key[KEY_LEFT]) { offset_x += scroll_speed; }
            else if (key[KEY_RIGHT]) { offset_x -= scroll_speed; }
            if (key[KEY_4_PAD]) { tile_offset += scroll_speed; }
            else if ( key[KEY_6_PAD]) { tile_offset -= scroll_speed; }
            if ( key[KEY_7_PAD]) { tile_offset = 0; }
            else if ( key[KEY_1_PAD]) { tile_offset = -((graphics->w) - 1024); }
            if (key[KEY_9_PAD]) { tile_offset -= scroll_speed*4; }
            else if (key[KEY_3_PAD]) { tile_offset += scroll_speed*4; }
            if (key[KEY_G]) { key_down = true; }
            if (!key[KEY_G] && key_down == true)
            {
                if (show_grid) { show_grid = false; }
                else { show_grid = true; }
                key_down = false;
            }
/*--------*/if (key[KEY_L])                                     //Load map
            {
                //loading = true;
                ifstream infile;
                string filename;
                cout<<"\nPlease enter desired filename (including extention): ";
                cout<<endl;
                cin>>filename;
                infile.open(filename.c_str());
                /*
                Map format -
                bottom layer
                top layer
                */
                //BOTTOM LAYER
                for (int k=0; k<3; k++)
                {
                    for (int j=0; j<max_y; j++)
                    {
                        for (int i=0; i<max_x; i++)
                        {
                            int temp;
                            infile>>temp;
                            grid[k][i][j].codex = temp*32;
                        }
                    }
                }
                infile.close();
                cout<<"Map loaded successfully";
                //loading = false;
            }
/*--------*/if (key[KEY_S])                                     //Save map
            {
                saving = true;
                ofstream outfile;
                string filename;
                cout<<"\nPlease enter desired filename (including extention): ";
                cout<<endl;
                cin>>filename;
                outfile.open(filename.c_str());
                /*
                Map format -
                bottom layer
                top layer
                */
                //BOTTOM LAYER
                for (int k=0; k<3; k++)
                {
                    for (int j=0; j<max_y; j++)
                    {
                        for (int i=0; i<max_x; i++)
                        {
                            if ( grid[k][i][j].codex/32 < 1000 )
                            {
                                if ( grid[k][i][j].codex/32 < 10 )
                                {
                                    outfile<<"000"<<grid[k][i][j].codex/32<<" ";
                                }
                                else if ( grid[k][i][j].codex/32 < 100 )
                                {
                                    outfile<<"00"<<grid[k][i][j].codex/32<<" ";
                                }
                                else if ( grid[k][i][j].codex/32 < 1000 )
                                {
                                    outfile<<"0"<<grid[k][i][j].codex/32<<" ";
                                }
                            }
                            else
                            {
                                outfile<<grid[k][i][j].codex/32<<" ";
                            }
                        }
                        outfile<<endl;
                    }
                    outfile<<endl;
                }
                cout<<filename<<" written successfully."<<endl;
                saving = false;
            }
            if ( mouse_b )
            {
                click = true;
                int mx, my;
                if ( mouse_y > TOOLBAR_H )
                {
                    mx = (mouse_x - 32 - offset_x)/32;  //no need for collision detection!
                    my = (mouse_y - 32 - offset_y)/32;
                    grid[layer][mx][my].codex = brushcode;
                    grid[layer][mx][my].codey = 0;
                    if ( brush_size == 3 )
                    {
                        grid[layer][mx+1][my-1].codex = brushcode;
                        grid[layer][mx][my-1].codex = brushcode;
                        grid[layer][mx-1][my-1].codex = brushcode;

                        grid[layer][mx+1][my].codex = brushcode;
                        grid[layer][mx-1][my].codex = brushcode;

                        grid[layer][mx+1][my+1].codex = brushcode;
                        grid[layer][mx][my+1].codex = brushcode;
                        grid[layer][mx-1][my+1].codex = brushcode;
                    }
                }
                click = true;
            }
            else
            {
                if ( click == true )
                {
                    if ( mouse_y < TOOLBAR_H && mouse_y > TOOLBAR_H - 35 )
                    {
                        int mx = ( mouse_x - tile_offset ) / 32;
                        brushcode = tileset[mx].codex;
                    } //if ( mouse_y < TOOLBAR_H && mouse_y > TOOLBAR_H - 32 )
                    else if ( mouse_y < TOOLBAR_H - 32 )
                    {
                        for (int btn = 0; btn<BUTTON_AMT; btn++)
                        {
                            if ( button[btn].check_click(mouse_x, mouse_y) )
                            {
                                if ( btn == 0 || btn == 1 || btn == 5 || btn == 6 || btn == 7 )
                                {
                                    button[btn].pressed = true;
                                } //if ( btn == 0 || btn == 1 || btn == 5 || btn == 6 || btn == 7 )
                                if (button[btn].text == "size 1")
                                {
                                    brush_size = 1;
                                    button[1].pressed = false;
                                } //if (button[btn].text == "size 1")
                                else if (button[btn].text == "size 3" )
                                {
                                    brush_size = 3;
                                    button[0].pressed = false;
                                }
                                else if (button[btn].text == "fill layer" )
                                {
                                    for (int j=0; j<max_y; j++)
                                    {
                                        for (int i=0; i<max_x; i++)
                                        {
                                            grid[layer][i][j].codex = brushcode;
                                        }
                                    }
                                }
                                else if (button[btn].text == "clear layer" )
                                {
                                    for (int j=0; j<max_y; j++)
                                    {
                                        for (int i=0; i<max_x; i++)
                                        {
                                            grid[layer][i][j].codex = 0;
                                        }
                                    }
                                }
                                else if (button[btn].text == "bottom layer" )
                                {
                                    layer = 0;
                                    button[6].pressed = false;
                                    button[7].pressed = false;
                                }
                                else if (button[btn].text == "middle layer" )
                                {
                                    layer = 1;
                                    button[5].pressed = false;
                                    button[7].pressed = false;
                                }
                                else if (button[btn].text == "top layer" )
                                {
                                    layer = 2;
                                    button[5].pressed = false;
                                    button[6].pressed = false;
                                }
/*-----------------------------*/else if (button[btn].text == "save" )      //save
                                {
                                    saving = true;
                                    ofstream outfile;
                                    string filename;
                                    cout<<"\nPlease enter desired filename (including extention): ";
                                    cin>>filename;
                                    cout<<endl;
                                    outfile.open(filename.c_str());
                                    /*
                                    Map format -
                                    bottom layer
                                    top layer
                                    */
                                    //BOTTOM LAYER
                                    for (int k=0; k<3; k++)
                                    {
                                        for (int j=0; j<max_y; j++)
                                        {
                                            for (int i=0; i<max_x; i++)
                                            {
                                                if ( grid[k][i][j].codex/32 < 1000 )
                                                {
                                                    if ( grid[k][i][j].codex/32 < 10 )
                                                    {
                                                        outfile<<"000"<<grid[k][i][j].codex/32<<" ";
                                                    }
                                                    else if ( grid[k][i][j].codex/32 < 100 )
                                                    {
                                                        outfile<<"00"<<grid[k][i][j].codex/32<<" ";
                                                    }
                                                    else if ( grid[k][i][j].codex/32 < 1000 )
                                                    {
                                                        outfile<<"0"<<grid[k][i][j].codex/32<<" ";
                                                    }
                                                }
                                                else
                                                {
                                                    outfile<<grid[k][i][j].codex/32<<" ";
                                                }
                                            }
                                            outfile<<endl;
                                        }
                                        outfile<<endl;
                                    }
                                    cout<<filename<<" written successfully."<<endl;
                                    saving = false;
                                }
/*-----------------------------*/else if (button[btn].text == "load" )      //load
                                {
                                    ifstream infile;
                                    string filename;
                                    cout<<"\nPlease enter desired filename (including extention): ";
                                    cout<<endl;
                                    cin>>filename;
                                    infile.open(filename.c_str());
                                    /*
                                    Map format -
                                    bottom layer
                                    top layer
                                    */
                                    //BOTTOM LAYER
                                    for (int k=0; k<3; k++)
                                    {
                                        for (int j=0; j<max_y; j++)
                                        {
                                            for (int i=0; i<max_x; i++)
                                            {
                                                int temp;
                                                infile>>temp;
                                                grid[k][i][j].codex = temp*32;
                                            }
                                        }
                                    }
                                    infile.close();
                                    cout<<"Map loaded successfully";
                                }
                                else if (button[btn].text == "exit" )
                                {
                                    done = true;
                                }
                            } //if ( button[btn].check_click(mouse_x, mouse_y) )
                        } //for (int btn = 0; btn<BUTTON_AMT; btn++)
                    } //else if ( mosue_y < TOOLBAR_H - 32 )
                } //if ( click == true )
                click = false;
            }
            speed_counter--;
        }//while (speed_counter > 0)
        vsync();
        //Draw bg
        rectfill(buffer, 0, 0, SCR_W, SCR_H, makecol(110, 65, 110));
        //Draw lower grid
        for (int j=0; j<max_y; j++)
        {
            for (int i=0; i<max_x; i++)
            {
                //if ( i*32+offset_x < 1024+32 && j*32+offset_y < 768+32 && i*32+offset_x > -32 && j*32+offset_y > -32 )
                if ( i*32+offset_x < 1024+32 && j*32+offset_y < 768+32 && i*32+offset_x > -32 && j*32+offset_y > -32 )
                {
                    for (int k=0; k<3; k++)
                    {
                        masked_blit(graphics, buffer, grid[k][i][j].codex, grid[k][i][j].codey, grid[k][i][j].x+offset_x, grid[k][i][j].y+offset_y, grid[k][i][j].w, grid[k][i][j].h);
                        if ( show_grid == true )
                        {
                            rect(buffer, grid[k][i][j].x+offset_x, grid[k][i][j].y+offset_y, grid[k][i][j].x+offset_x+grid[k][i][j].w, grid[k][i][j].y+offset_y+grid[k][i][j].h, makecol(50, 0, 0));
                        }
                    }
                }
            }
        }
        for (int i=1; i<= max_x; i++)
        {
            textprintf_centre(buffer, font, i*32+16+offset_x, 150+16, makecol(255, 255, 255), "%i", i-1);
        }
        for (int j=0; j <= max_y; j++)
        {
            textprintf_centre(buffer, font, 10, j*32+16+offset_y, makecol(255, 255, 255), "%i", j-1);
        }
        //Draw toolbar & text
        rectfill(buffer, 0, 0, SCR_W, TOOLBAR_H, makecol(120, 190, 120));
        line(buffer, 0, TOOLBAR_H, SCR_W, TOOLBAR_H, makecol(50, 25, 50));
        //draw tileset brushes
        for (int i=0; i<tileset_amt; i++)
        {
            masked_blit(graphics, buffer, tileset[i].codex, tileset[i].codey, tileset[i].x+tile_offset, tileset[i].y, tileset[i].w, tileset[i].h);
            rect(buffer, tileset[i].x+tile_offset, tileset[i].y, tileset[i].x+tileset[i].w+tile_offset, tileset[i].y+tileset[i].h, makecol(0, 0, 0));
        }
        //instructions
        textprintf(buffer, font, 349, 15, makecol(25, 75, 25), "To scroll the map, press the arrow keys.");
        textprintf(buffer, font, 349, 25, makecol(25, 75, 25), "To scroll the tileset, press the numpad keys, with HOME(7), END(1),");
        textprintf(buffer, font, 349, 35, makecol(25, 75, 25), "PG UP(9) and PG DN(3).");
        textprintf(buffer, font, 349, 45, makecol(25, 75, 25), "Press 'G' to toggle the grid");
        textprintf(buffer, font, 349, 55, makecol(25, 75, 25), "'S' and 'L' are keyboard shortcuts for Save and Load");
        textprintf(buffer, font, 349, 65, makecol(25, 75, 25), "Press F5 to toggle fullscreen, F4 to quit");
        textprintf(buffer, font, 349, 85, makecol(150, 75, 25), "WHEN SAVING OR LOADING, GO TO THE PROMPT TO ENTER A FILENAME.");
        textprintf(buffer, font, 349, 95, makecol(150, 75, 25), "THE MAP EDITOR FREEZES UNTIL YOU DO.");
        textprintf_centre(buffer, font, SCR_W/2, 2, makecol(0, 0, 0), "MusuGo Map Editor v1.1 by Rachel J. Morris");
        //Draw current brush
        masked_blit(graphics, buffer, brushcode, 0, 960, 8, 32, 32);
        rect(buffer, 959, 7, 959+34, 7+34, makecol(0, 0, 0));
        //draw buttons
        for (int i=0; i<BUTTON_AMT; i++)
        {
            if ( button[i].pressed )
                rectfill(buffer, button[i].x, button[i].y, button[i].x+button[i].w, button[i].y+button[i].h, makecol(65, 125, 65));
            else
                rectfill(buffer, button[i].x, button[i].y, button[i].x+button[i].w, button[i].y+button[i].h, makecol(160, 235, 150));
            rect(buffer, button[i].x, button[i].y, button[i].x+button[i].w, button[i].y+button[i].h, makecol(255, 255, 255));
            textprintf_centre(buffer, font, button[i].x+button[i].w/2, button[i].y+button[i].h/2, makecol(0, 0, 0), button[i].text.c_str() );
        }
        //Draw cursor
        if ( click == true )
        {
            line(buffer, mouse_x-10, mouse_y, mouse_x+10, mouse_y, makecol(255, 50, 50));
            line(buffer, mouse_x, mouse_y-10, mouse_x, mouse_y+10, makecol(255, 50, 50));
        }
        else
        {
            line(buffer, mouse_x-10, mouse_y, mouse_x+10, mouse_y, makecol(50, 255, 255));
            line(buffer, mouse_x, mouse_y-10, mouse_x, mouse_y+10, makecol(50, 255, 255));
        }
        if ( saving )
        {
            textprintf_centre(buffer, font, SCR_W/2, SCR_H/2, makecol(255, 0, 0), "PLEASE GO TO THE DOS PROMPT TO ENTER A FILENAME.");
        }
//        else
//        {
//            textprintf_centre(buffer, font, SCR_W/2, SCR_H/2, makecol(255, 0, 0), "PLEASE GO TO THE DOS PROMPT TO ENTER A FILENAME.");
//        }
        if ( loading )
        {
            textprintf_centre(buffer, font, SCR_W/2, SCR_H/2, makecol(255, 0, 0), "PLEASE GO TO THE DOS PROMPT TO ENTER A FILENAME.");
        }
        acquire_screen();
        blit(buffer, screen, 0, 0, 0, 0, SCR_W, SCR_H);
        clear_bitmap(buffer);
        release_screen();
    }//while (!done)
    return 0;
}
END_OF_MAIN();

void init_allegro()
{
    allegro_init();
    install_keyboard();
    install_timer();
    install_mouse();
    install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0);
    LOCK_VARIABLE(speed_counter);
    LOCK_FUNCTION(increment_speed_counter);
    install_int_ex(increment_speed_counter,BPS_TO_TIMER(90));
    set_color_depth(16);
    set_gfx_mode(GFX_AUTODETECT_WINDOWED, SCR_W, SCR_H, 0, 0);
    //set_gfx_mode(GFX_AUTODETECT, scr_w, scr_h, 0, 0);
    text_mode(-1);
}

void increment_speed_counter()
{
    speed_counter++;
}