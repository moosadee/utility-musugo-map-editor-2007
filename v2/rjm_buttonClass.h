#pragma once
#include <string.h>
using namespace std;

class rjm_buttonClass
{
    public:
    int x, y, w, h;
    bool pressed;
    string text;
    bool check_click(int mouse_x, int mouse_y)
    {
        if (    (mouse_x > x) && (mouse_x < x+w) &&
                (mouse_y > y) && (mouse_y < y+h) )
        {
            //clicked
            return true;
        }
        return false;
    }
    void init(int index)
    {
        pressed = false;
        switch (index)
        {
            case 0:
                x = 0;
                y = 0;
                w = 100;
                h = 25;
                pressed = true;
                text = "size 1";
                break;
            case 1:
                x = 0;
                y = 28;
                w = 100;
                h = 25;
                text = "size 3";
                break;
            case 2:
                x = 0;
                y = 56;
                w = 100;
                h = 25;
                text = "fill layer";
                break;
            case 3:
                x = 210;
                y = 0;
                w = 100;
                h = 25;
                text = "save";
                break;
            case 4:
                x = 210;
                y = 28;
                w = 100;
                h = 25;
                text = "load";
                break;
            case 5:
                x = 105;
                y = 0;
                w = 100;
                h = 25;
                pressed = true;
                text = "bottom layer";
                break;
            case 6:
                x = 105;
                y = 28;
                w = 100;
                h = 25;
                text = "middle layer";
                break;
            case 7:
                x = 105;
                y = 56;
                w = 100;
                h = 25;
                text = "top layer";
                break;
            case 8:
                x = 210;
                y = 56;
                w = 100;
                h = 25;
                text = "exit";
                break;
            case 9:
                x = 0;
                y = 84;
                w = 100;
                h = 25;
                text = "clear layer";
                break;
        }
    }
};